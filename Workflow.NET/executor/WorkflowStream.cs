﻿using executor.Interfaces;

namespace executor
{
    internal class WorkflowStream : IStream
    {
        public string Data { get; }

        public WorkflowStream(string data)
        {
            Data = data;
        }

        public static implicit operator string(WorkflowStream stream)  
        {
            return stream.Data; 
        }
    }
}