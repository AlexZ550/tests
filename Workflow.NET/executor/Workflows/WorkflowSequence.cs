﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace executor.Workflows
{
    class WorkflowSequence : List<string>
    {
        public WorkflowSequence(string workflowDefinition) 
        {
            this.AddRange(workflowDefinition.Replace("->", "!").Split('!').ToList());
        }

        public override string ToString()
        {
            return String.Join(",", this);
        }
    }
}
