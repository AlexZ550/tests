﻿using System;

namespace executor.Workflows
{
    internal class WorkflowFactory
    {
        internal static WorkflowSequence CreateSequence(string workflowDefinition)
        {
            return new WorkflowSequence(workflowDefinition);
        }
    }
}