﻿using System;
using System.Collections.Generic;
using executor.Interfaces;
using executor.Workflows;

namespace executor
{
    internal class Job
    {
        private WorkflowSequence sequence;
        private Dictionary<string, ICommand> commandList;

        public Job(WorkflowSequence sequence, Dictionary<string, ICommand> commandList)
        {
            this.sequence = sequence;
            this.commandList = commandList;
        }

        internal void Run()
        {
            IStream stream = new WorkflowStream(String.Empty);
            foreach (var id in sequence)
            {
                var command = commandList[id];
                stream = command.execute(stream);
            }
            ExecutionBarrier.RemoveParticipant();
        }
    }
}