﻿using System;
using System.Threading;

namespace executor
{
    internal class ExecutionBarrier
    {
        static Barrier _barrier = new Barrier(0);

        internal static void SignalAndWait()
        {
            _barrier.SignalAndWait();
        }

        internal static void RemoveParticipant()
        {
            _barrier.RemoveParticipant();
        }

        internal static void AddParticipants(int count)
        {
            _barrier.AddParticipants(count);
        }
    }
}