﻿namespace executor.Interfaces
{
    internal interface ICommand
    {
        string Id { get;  }
        IStream execute(IStream inStream);
    }
}