﻿namespace executor.Interfaces
{
    public interface IStream
    {
        string Data { get; }
    }
}