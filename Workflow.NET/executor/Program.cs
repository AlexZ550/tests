﻿using System;

namespace executor
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("Usage: executor workflow.txt");
                return;
            }

            WorkflowExecutor wf = new WorkflowExecutor(args[0]);
            wf.Run();
        }

    }
}
