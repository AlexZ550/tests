﻿using executor.Interfaces;
using System;

namespace executor.Commands
{
    internal class ReplaceCommand : Command
    {
        private string _sourceText;
        private string _targetText;

        public ReplaceCommand(string commandLine) : base(commandLine)
        {
            var parts = _commandLine.Split(' ');
            _sourceText = parts[1].Trim();
            _targetText = parts[2].Trim();
        }

        public override IStream execute(IStream inStream)
        {
            Console.WriteLine("Replacing {0} with {1}", _sourceText, _targetText);
            return new WorkflowStream(inStream.Data.Replace(_sourceText, _targetText));
        }
    }
}