﻿using executor.Interfaces;
using System;

namespace executor.Commands
{
    internal class Command : ICommand
    {
        protected string _commandLine;

        public Command(string commandLine)
        {
            var parts = commandLine.Split('=');
            if (parts.Length < 2)
                throw new ArgumentOutOfRangeException(commandLine, "Incorrect command format");

            this.Id = parts[0].Replace(".", String.Empty).Trim();
            _commandLine = parts[1].Trim();
        }

        public string Id { get; }

        public virtual IStream execute(IStream inStream)
        {
            throw new System.NotImplementedException();
        }
    }
}