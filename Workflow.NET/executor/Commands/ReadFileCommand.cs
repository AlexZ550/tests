﻿using executor.Interfaces;
using System;
using System.IO;

namespace executor.Commands
{
    internal class ReadFileCommand : Command
    {
        private string _fileName;

        public ReadFileCommand(string commandLine) : base(commandLine)
        {
            var parts = _commandLine.Split(' ');
            _fileName = parts[1].Trim();
        }

        public override IStream execute(IStream inStream)
        {
            Console.WriteLine("Reading a file {0}.", _fileName);
            var data = File.ReadAllText(_fileName);
            return new WorkflowStream(data);
        }

    }
}