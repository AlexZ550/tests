﻿using executor.Interfaces;
using System;

namespace executor.Commands
{
    internal class SynchCommand : Command
    {
        public SynchCommand(string commandLine) : base(commandLine)
        {
        }

        public override IStream execute(IStream inStream)
        {
            Console.WriteLine("Synchronizing");
            ExecutionBarrier.SignalAndWait();
            Console.WriteLine("Released");
            return inStream;
        }

    }
}