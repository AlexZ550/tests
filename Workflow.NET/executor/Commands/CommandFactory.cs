﻿using executor.Interfaces;
using System;

namespace executor.Commands
{
    internal class CommandFactory 
    {
        internal static ICommand CreateCommand(string commandLine)
        {
            if (commandLine.Contains("replace"))
                return new ReplaceCommand(commandLine);
            if (commandLine.Contains("writefile"))
                return new WriteFileCommand(commandLine);
            if (commandLine.Contains("readfile"))
                return new ReadFileCommand(commandLine);
            if (commandLine.Contains("sort"))
                return new SortCommand(commandLine);
            if (commandLine.Contains("grep"))
                return new GrepCommand(commandLine);
            if (commandLine.Contains("synch"))
                return new SynchCommand(commandLine);
            throw new ArgumentOutOfRangeException(commandLine, "Command is not implemented");
        }
    }
}