﻿using executor.Interfaces;
using System;
using System.Collections.Generic;

namespace executor.Commands
{
    internal class GrepCommand : Command
    {
        private string _text;

        public GrepCommand(string commandLine) : base(commandLine)
        {
            var parts = _commandLine.Trim().Split(' ');
            _text = parts[1].Trim();
        }

        public override IStream execute(IStream inStream)
        {
            Console.WriteLine("Grep {0}", _text);
            return new WorkflowStream(String.Join("\n", grep(inStream.Data)));
        }

        private IEnumerable<string> grep(string data)
        {
            var lines = data.Split('\n');
            foreach (var line in lines)
                if (line.Contains(_text))
                    yield return line;
        }

    }
}