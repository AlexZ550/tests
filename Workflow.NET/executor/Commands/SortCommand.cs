﻿using executor.Interfaces;
using System;
using System.Linq;

namespace executor.Commands
{
    internal class SortCommand : Command
    {
        public SortCommand(string commandLine) : base(commandLine)
        {
        }

        public override IStream execute(IStream inStream)
        {
            Console.WriteLine("Sorting");

            return new WorkflowStream(String.Join("\n",
                  inStream.Data.Split('\n').OrderBy(x => x)
                ));
        }


    }
}