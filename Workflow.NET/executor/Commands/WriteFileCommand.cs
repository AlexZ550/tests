﻿using executor.Interfaces;
using System;
using System.IO;

namespace executor.Commands
{
    internal class WriteFileCommand : Command
    {
        private string _fileName;

        public WriteFileCommand(string commandLine) : base(commandLine)
        {
            var parts = _commandLine.Split(' ');
            _fileName = parts[1].Trim();
        }

        public override IStream execute(IStream inStream)
        {
            Console.WriteLine("Writing a file {0}.", _fileName);
            File.WriteAllText(_fileName, inStream.Data);
            return inStream;
        }
    }
}