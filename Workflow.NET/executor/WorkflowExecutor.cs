﻿using executor.Commands;
using executor.Interfaces;
using executor.Workflows;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace executor
{
    internal class WorkflowExecutor
    {
        const int TITLE = 0;
        private string _workflowFile;
        Dictionary<string, ICommand> commandList = new Dictionary<string, ICommand>();
        List<WorkflowSequence> sequences = new List<WorkflowSequence>();

        public WorkflowExecutor(string workflowFile)
        {
            _workflowFile = workflowFile;
            initialize();
        }

        private void initialize()
        {
            var lines = File.ReadAllLines(_workflowFile);

            Console.WriteLine("Loading the workflow from: {0}", lines[TITLE]);

            var index = 1;
            while (lines[index] != "end")
            {
                var command = CommandFactory.CreateCommand(lines[index++]);
                commandList.Add(command.Id, command);
            }

            //Skip end delemiter
            index++;

            while (index < lines.Length )
            {
                sequences.Add(WorkflowFactory.CreateSequence(lines[index++]));
            }

        }

        internal void Run()
        {
            Console.WriteLine("Execution started");
            ExecutionBarrier.AddParticipants(sequences.Count);
            var tasks = new List<Task>();
            foreach (var seq in sequences)
                    tasks.Add(Task.Factory.StartNew(() => {
                        var job = new Job(seq, commandList);
                        job.Run();
                }));
            Task.WaitAll(tasks.ToArray());

            Console.WriteLine("Execution finished");
        }
    }
}