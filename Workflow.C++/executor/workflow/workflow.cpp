// executor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "../executor/WorkflowExecutor.h"
using namespace std;

int main(int argc, char* argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " workflow.txt" << endl;
		return 1;
	}
	try
	{
		auto wf = new WorkflowExecutor(argv[1]);
		wf->run();
	}
	catch (exception& ex)
	{
		cout << ex.what() << endl;
	}
	catch (...)
	{
		cout << "Unhandled exception" << endl;
	}

	return 0;
}


