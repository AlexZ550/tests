#include "stdafx.h"
#include "CppUnitTest.h"
#include "../executor/WorkflowFactory.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ExecutorUnitTest
{		
	TEST_CLASS(UnitTests)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			auto wf = &WorkflowFactory::createSequence(std::string("1->2->3"));
			Assert::IsNotNull(wf);
		}

	};
}