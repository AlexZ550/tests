#include "stdafx.h"
#include <mutex>
#include "Barrier.h"

Barrier* Barrier::instance = nullptr;

Barrier::Barrier(std::size_t count):_count(count), _original_count(count)
{
	instance = this;
}

Barrier* Barrier::getInstance()
{
	if (!instance)
		instance = new Barrier(0);
	return instance;
}

void Barrier::SignalAndWait()
{
	std::unique_lock<std::mutex> lock(_mutex);
	if (--_count == 0) {
		_cv.notify_all();
		_count = _original_count;
	}
	else {
		if (_original_count>1)
		   _cv.wait(lock);
	}
}

void Barrier::RemoveParticipant()
{
	std::unique_lock<std::mutex> lock(_mutex);
	if (--_original_count == 1) {
		_cv.notify_all();
	}
}
