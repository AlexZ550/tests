#include "stdafx.h"
#include <fstream>
#include "ReadFileCommand.h"
#include "IStream.h"
#include "Utils.h"
#include <iostream>


ReadFileCommand::ReadFileCommand()
{
}

ReadFileCommand::ReadFileCommand(const std::string& commandText) : Command(commandText)
{
	auto parts = Utils::split_string(_commandLine, " ");
	if (parts.size() < 2)
		throw std::invalid_argument("Invalid command line" + commandText);
	_fileName = parts[1];
}

IStream& ReadFileCommand::execute(IStream& inStream)
{
	std::cout << "Loading file " << _fileName << std::endl;
	std::ifstream fs(_fileName);
	std::string content((std::istreambuf_iterator<char>(fs)),
		(std::istreambuf_iterator<char>()));
	fs.close();
	inStream.assign(content);
	return inStream;
}


ReadFileCommand::~ReadFileCommand()
{
}
