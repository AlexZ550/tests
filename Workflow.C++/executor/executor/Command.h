#pragma once
#include "ICommand.h"
class Command :
	public ICommand
{
protected:
	std::string _commandLine;
public:
	Command();
	Command(const std::string& commandText);
	~Command();
	IStream& execute(IStream& inStream) override;
};

