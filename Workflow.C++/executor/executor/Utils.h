#pragma once
#include <string>
#include <vector>

class Utils
{
public:
	static std::vector<std::string> split_string(std::string content, std::string delimeter);
	static std::string trim(const std::string &s);
};

