#pragma once
#include "Command.h"
class WriteFileCommand :
	public Command
{
protected:
	std::string _fileName;
public:
	WriteFileCommand();
	WriteFileCommand(const std::string& commandText);
	~WriteFileCommand();
	IStream& execute(IStream& inStream) override;
};

