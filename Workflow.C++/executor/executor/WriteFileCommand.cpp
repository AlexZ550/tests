#include "stdafx.h"
#include <fstream>
#include "WriteFileCommand.h"
#include "IStream.h"
#include <iostream>
#include "Utils.h"


WriteFileCommand::WriteFileCommand()
{
}

WriteFileCommand::WriteFileCommand(const std::string & commandText):Command(commandText)
{
	auto parts = Utils::split_string(_commandLine, " ");
	if (parts.size() < 2)
		throw std::invalid_argument("Invalid command line" + commandText);
	_fileName = parts[1];
}


WriteFileCommand::~WriteFileCommand()
{

}

IStream& WriteFileCommand::execute(IStream& inStream)
{
	std::cout << "Writing into a file " << _fileName << std::endl;
	std::ofstream fs(_fileName);
	fs << inStream;
	fs.close();
	return inStream;
}
