#pragma once
#include <ostream>
#include <vector>

class WorkflowSequence:
   public std::vector<std::string>
{
protected:
	std::string _sequenceText;
public:
	WorkflowSequence();
	~WorkflowSequence();
	WorkflowSequence(std::string& sequenceText);
	friend std::ostream &operator<<(std::ostream &os, const WorkflowSequence& sequence);
};

