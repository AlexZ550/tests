#include "stdafx.h"
#include "CommandFactory.h"
#include <string>
#include "Utils.h"
#include "ReplaceCommand.h"
#include "WriteFileCommand.h"
#include "ReadFileCommand.h"
#include "SortCommand.h"
#include "GrepCommand.h"
#include "SynchCommand.h"
#include <memory>


CommandFactory::CommandFactory()
{
}


CommandFactory::~CommandFactory()
{
}

Command* CommandFactory::createCommand(const std::string& commandText)
{
	if (commandText.find("replace") != std::string::npos)
		return new ReplaceCommand(commandText);
	if (commandText.find("writefile") != std::string::npos)
		return new WriteFileCommand(commandText);
	if (commandText.find("readfile") != std::string::npos)
		return  new ReadFileCommand(commandText);
	if (commandText.find("sort") != std::string::npos)
		return new SortCommand(commandText);
	if (commandText.find("grep") != std::string::npos)
		return new GrepCommand(commandText);
	if (commandText.find("synch") != std::string::npos)
		return new SynchCommand(commandText);
	throw std::invalid_argument(commandText + "Command is not implemented");
}
