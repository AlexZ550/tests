#pragma once
#include "WorkflowSequence.h"
#include <list>
#include "Command.h"
#include <map>
#include <memory>

class Job
{
public:
	Job();
	~Job();
	void run();
	void operator()(const WorkflowSequence& seq, const std::map<std::string, Command* >& commandList);
};

