#include "stdafx.h"
#include "GrepCommand.h"
#include "IStream.h"
#include "Utils.h"
#include <numeric>
#include <iostream>


GrepCommand::GrepCommand()
{
}

GrepCommand::GrepCommand(const std::string& command_text):Command(command_text)
{
	auto parts = Utils::split_string(_commandLine, " ");
	if (parts.size() < 2)
		throw std::invalid_argument("Invalid command line" + command_text);
	_text = parts[1];
}


GrepCommand::~GrepCommand()
{
}

IStream& GrepCommand::execute(IStream& inStream)
{
	std::cout << "Grepping " << _text << std::endl;
	auto lines = Utils::split_string(inStream, "\n");
	std::vector<std::string> out;
	for (const auto& line : lines)
	{
		if (line.find(_text))
		{
			out.push_back(line+"\n");
		}	
	}
	inStream.assign(std::accumulate(out.begin(), out.end(), std::string{}));
	return inStream;
}
