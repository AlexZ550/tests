#include "stdafx.h"
#include "Command.h"
#include "Utils.h"
#include <algorithm>
#include "IStream.h"
#include <iostream>


Command::Command()
{
}

Command::Command(const std::string& commandText)
{
	auto parts = Utils::split_string(commandText, "=");
	if (parts.size() < 2)
		throw std::invalid_argument("Invalid command line" + commandText);
	Id = Utils::trim(parts[0]);
	_commandLine = Utils::trim(parts[1]);
}


Command::~Command()
{
}

IStream& Command::execute(IStream& inStream)
{
	std::cout << "Command not implemented " << std::endl;
	return inStream;
}
