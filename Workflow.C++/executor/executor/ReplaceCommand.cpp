#include "stdafx.h"
#include "ReplaceCommand.h"
#include "Utils.h"
#include "IStream.h"
#include <iostream>
#include <algorithm>
#include <regex>


ReplaceCommand::ReplaceCommand()
{
}

ReplaceCommand::ReplaceCommand(const std::string& commandText):Command(commandText)
{
	auto parts = Utils::split_string(_commandLine, " ");
	if (parts.size() < 2)
		throw std::invalid_argument("Invalid command line" + commandText);
	_sourceText = Utils::trim(parts[1]);
	_targetText = Utils::trim(parts[2]);
}

ReplaceCommand::~ReplaceCommand()
{
}

IStream& ReplaceCommand::execute(IStream& inStream)
{
	std::cout << "Replacing " << _sourceText << " with " << _targetText << std::endl;
	inStream.assign(std::regex_replace(inStream, std::regex(_sourceText), _targetText));
	return inStream;
}
