#pragma once
#include <csignal>
#include <mutex>

class Barrier
{
	std::mutex _mutex;
	std::condition_variable _cv;
	std::size_t _count;
	std::size_t _original_count;
	static Barrier* instance;

public:
	static Barrier* getInstance();
	Barrier(std::size_t count);
	void SignalAndWait();
	void RemoveParticipant();
};


