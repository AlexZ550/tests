#pragma once
#include <string>
#include "Command.h"
#include <memory>

class CommandFactory
{
public:
	CommandFactory();
	~CommandFactory();
	static Command* createCommand(const std::string& commandText);
};

