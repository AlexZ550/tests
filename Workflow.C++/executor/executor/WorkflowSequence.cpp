#include "stdafx.h"
#include "WorkflowSequence.h"
#include <iostream>
#include "Utils.h"


WorkflowSequence::WorkflowSequence(std::string& sequenceText) : 
	std::vector<std::string>(Utils::split_string(sequenceText, "->"))
{
	_sequenceText = sequenceText;
	
}

WorkflowSequence::WorkflowSequence()
{
}


WorkflowSequence::~WorkflowSequence()
{
}


std::ostream& operator<<(std::ostream& os, const WorkflowSequence& sequence)
{
	os << sequence._sequenceText.c_str();
	return os;
}

