#include "stdafx.h"
#include "SynchCommand.h"
#include "IStream.h"
#include <iostream>
#include "Barrier.h"


SynchCommand::SynchCommand()
{
}

SynchCommand::SynchCommand(const std::string& command_text):Command(command_text)
{
}


SynchCommand::~SynchCommand()
{
}

IStream& SynchCommand::execute(IStream& inStream)
{
	std::cout << "Waiting to synch up" << std::this_thread::get_id() << std::endl;
	Barrier::getInstance()->SignalAndWait();
	std::cout << "Released" << std::this_thread::get_id() <<std::endl;
	return inStream;
}
