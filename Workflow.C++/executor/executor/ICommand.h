#pragma once
#include <string>

class IStream;

class ICommand
{
public:
	ICommand();
	~ICommand();
	std::string Id;
	virtual IStream& execute(IStream& inStream) = 0;
};

