#include "stdafx.h"
#include "Job.h"
#include <iostream>
#include "IStream.h"
#include <memory>
#include "Barrier.h"


Job::Job()
{
}

Job::~Job()
{
}

void Job::operator()(const WorkflowSequence& seq, const std::map<std::string, Command*>& commandList)
{
	std::cout << "Sequence " << seq << " executed" << std::endl;
	IStream stream;
	for (auto id : seq)
	{
		Command* command = commandList.at(id);
		stream = command->execute(stream);
	}
	Barrier::getInstance()->RemoveParticipant();
}
