#pragma once
#include "Command.h"
class SortCommand :
	public Command
{
public:
	SortCommand();
	SortCommand(const std::string& command_text);
	~SortCommand();
	IStream& execute(IStream& inStream) override;
};

