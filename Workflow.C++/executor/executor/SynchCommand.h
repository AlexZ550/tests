#pragma once
#include "Command.h"
class SynchCommand :
	public Command
{
public:
	SynchCommand();
	SynchCommand(const std::string& command_text);
	~SynchCommand();
	IStream& execute(IStream& inStream) override;
};

