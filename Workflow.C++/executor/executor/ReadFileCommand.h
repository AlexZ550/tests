#pragma once
#include "Command.h"
class ReadFileCommand :
	public Command
{
protected:
	std::string _fileName;
public:
	ReadFileCommand();
	ReadFileCommand(const std::string& commandText);
	~ReadFileCommand();
	IStream& execute(IStream& inStream) override;
};

