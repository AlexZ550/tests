#pragma once
#include <string>
#include <vector>
#include "WorkflowSequence.h"


class WorkflowFactory
{
public:
	WorkflowFactory();
	~WorkflowFactory();
	static WorkflowSequence createSequence(std::string& wfd);
};

