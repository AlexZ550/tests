#pragma once
#include "Command.h"
class ReplaceCommand :
	public Command
{
protected:
	std::string _sourceText;
	std::string _targetText;

	ReplaceCommand();
public:
	ReplaceCommand(const std::string& commandText);
	~ReplaceCommand();
	IStream& execute(IStream& inStream) override;
};

