#include "stdafx.h"
#include <fstream>
#include "WorkflowExecutor.h"
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include <map>
#include "ICommand.h"
#include "CommandFactory.h"
#include <list>
#include "WorkflowSequence.h"
#include "WorkflowFactory.h"
#include "Utils.h"
#include <thread>
#include "Job.h"
#include "Barrier.h"

using namespace std;

map<string, Command*> commandList;
list<WorkflowSequence> sequences;

WorkflowExecutor::WorkflowExecutor()
= default;

WorkflowExecutor::WorkflowExecutor(char * fileName)
{
	const int C_TITLE = 0;
	ifstream workflowFile(fileName);
	string content((istreambuf_iterator<char>(workflowFile)),
		(istreambuf_iterator<char>()));
	auto lines = Utils::split_string(content, "\n");

	cout << "Loading the workflow from: " << lines[C_TITLE] << endl;

	unsigned index = 1;

	while (lines[index] != "end")
	{
		auto command = CommandFactory::createCommand(lines[index++]);
		commandList[command->Id] = command;
	}

	index++;
	
	while (index < lines.size())
	{
		sequences.push_back(WorkflowFactory::createSequence(lines[index++]));
	}

}

WorkflowExecutor::~WorkflowExecutor()
= default;

void WorkflowExecutor::run() 
{
	cout << "Execution started" << endl;
	Barrier barrier(sequences.size());
	std::vector<std::thread> tasks;
	for (const auto& seq : sequences)
	{
		Job job;
		tasks.emplace_back((job), seq, commandList);
	}
	std::for_each(tasks.begin(), tasks.end(), std::mem_fn(&std::thread::join));
	cout << "Execution finished" << endl;
}

