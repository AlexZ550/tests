#pragma once
#include "Command.h"
class GrepCommand :
	public Command
{
protected:
	std::string _text;
public:
	GrepCommand();
	GrepCommand(const std::string& command_text);
	~GrepCommand();
	IStream& execute(IStream& inStream) override;
};

