#include "stdafx.h"
#include "SortCommand.h"
#include "IStream.h"
#include "Utils.h"
#include <algorithm>
#include <numeric>
#include <iostream>


SortCommand::SortCommand()
{
}

SortCommand::SortCommand(const std::string & command_text) : Command(command_text)
{
}


SortCommand::~SortCommand()
{
}

IStream& SortCommand::execute(IStream& inStream)
{
	std::cout << "Sorting" << std::endl;
	auto lines = Utils::split_string(inStream, "\n");
	std::sort(lines.begin(), lines.end());
	for (auto& line : lines)
	{
		line += "\n";
	}
	inStream.assign(std::accumulate(lines.begin(), lines.end(), std::string{}));
	return inStream;
}
