#include "stdafx.h"
#include "Utils.h"
#include <algorithm>
#include <string>
#include <vector>
#include <cctype>


std::vector<std::string> Utils::split_string(std::string content, std::string delimeter)
{
	std::vector<std::string> result;
	auto prev_pos = content.begin();
	auto next_pos = std::search(prev_pos, content.end(),
		delimeter.begin(), delimeter.end());
	while (next_pos != content.end())
	{
		result.emplace_back(prev_pos, next_pos);
		prev_pos = next_pos + delimeter.size();
		next_pos = std::search(prev_pos, content.end(),
			delimeter.begin(), delimeter.end());
	}

	if (prev_pos != content.end())
	{
		result.emplace_back(prev_pos, content.end());
	}
	return result;
}

std::string Utils::trim(const std::string &s)
{
	auto wsfront = std::find_if_not(s.begin(), s.end(), [](int c) {return std::isspace(c); });
	auto wsback = std::find_if_not(s.rbegin(), s.rend(), [](int c) {return std::isspace(c); }).base();
	return (wsback <= wsfront ? std::string() : std::string(wsfront, wsback));
}
