#pragma once
#include <string>

class IStream :
	public std::string
{
public:
	IStream();
	IStream(const std::string& buffer);
	~IStream();
};

