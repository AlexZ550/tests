﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalculatorV2
{
    public enum TokenTypes
    {
      NOT_INITIALIZED,
      NAME,
      NUMBER,
      END,
      PLUS = '+',
      MINUS = '-',
      MUL = '*',
      DIV = '/',
      PRINT = ';',
      ASSIGN = '=',
      LP = '(',
      RP = ')'
    };
}
