﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CalculatorV2
{
    public class Primary
    {
        private static Dictionary<string, double> variables = new Dictionary<string, double>();
        public static double Evaluate(TextReader inStream)
        {
            double value;
            switch (Token.Current)
            {
                case TokenTypes.NUMBER:
                    value = Token.Number;
                    Token.Next(inStream);
                    return value;
                case TokenTypes.NAME:
                    var name = Token.Name;
                    if (Token.Next(inStream) == TokenTypes.ASSIGN)
                    {
                        Token.Next(inStream);
                        variables[name] = Expression.Evaluate(inStream);
                        return variables[name];
                    }
                    return variables[name];
                case TokenTypes.MINUS:
                    value = Token.Number;
                    Token.Next(inStream);
                    return -1 * value;
                case TokenTypes.LP:
                    Token.Next(inStream);
                    var e = Expression.Evaluate(inStream); 
                    if (Token.Current != TokenTypes.RP)
                        throw new ArgumentException(") is expected");
                    Token.Next(inStream);
                    return e;
                case TokenTypes.END:
                    return 1;
                case TokenTypes.PRINT:
                    return 0;
                default:
                    throw new ArgumentException("value is expected");
            }
        }
    }
}