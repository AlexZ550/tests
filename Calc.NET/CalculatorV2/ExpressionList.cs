﻿using System;
using System.IO;

namespace CalculatorV2
{
    public class ExpressionList
    {
        public static void Evaluate(TextReader inStream, TextWriter outStream)
        {
            Token.Next(inStream);
            if (Token.Current == TokenTypes.PRINT)
                Evaluate(inStream, outStream);
            if (Token.Current != TokenTypes.END)
            {
                try
                {
                    outStream.WriteLine(Expression.Evaluate(inStream));
                }
                catch (Exception ex)
                {
                    outStream.WriteLine(ex.Message);
                    outStream.WriteLine(inStream.ReadToEnd());
                    return;
                }
                Evaluate(inStream, outStream);
            }
        }
    }
}