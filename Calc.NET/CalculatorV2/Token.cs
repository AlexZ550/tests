﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CalculatorV2
{
    public class Token
    {
        public static double Number { get; private set; }
        public static string Name { get; private set; }
        public static TokenTypes Current { get; private set; }

        public static TokenTypes Next(TextReader inStream)
        {
            Current = getNextToken(inStream);
            return Current;
        }

        private static TokenTypes getNextToken(TextReader inStream)
        {
            int ch;
            do
            {
                ch = inStream.Read();
            } while (ch != -1 && ch != '\n' && char.IsWhiteSpace((char)ch));

            switch (ch)
            {
                case -1:
                    return TokenTypes.END;
                case ';':
                case '\n':
                    return TokenTypes.PRINT;
                case '*':
                case '/':
                case '+':
                case '-':
                case '(':
                case ')':
                case '=':
                    return (TokenTypes)Enum.Parse(typeof(TokenTypes), ch.ToString());
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                    string value = string.Empty;
                    do
                    {
                        value += (char)ch;
                        ch = inStream.Peek();
                        if (char.IsDigit((char)ch) || ch == '.')
                            ch = inStream.Read();
                    } while (char.IsDigit((char)ch) || ch == '.');
                    Number = double.Parse(value);
                    return TokenTypes.NUMBER;
                default:
                    if (char.IsLetter((char)ch))
                    {
                        Name = string.Empty;
                        do
                        {
                            Name += (char)ch;
                            ch = inStream.Peek();
                            if (char.IsLetter((char)ch))
                                ch = inStream.Read();
                        }
                        while (char.IsLetter((char)ch));
                        return TokenTypes.NAME;
                    }
                    return TokenTypes.PRINT;
            }
        }
    }
}