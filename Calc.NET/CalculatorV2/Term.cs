﻿using System;
using System.IO;

namespace CalculatorV2
{
    public class Term
    {
        public static double Evaluate(TextReader inStream)
        {
            var left = Primary.Evaluate(inStream);

            while (true)
            {
                switch (Token.Current)
                {
                    case TokenTypes.MUL:
                        Token.Next(inStream);
                        left = left * Primary.Evaluate(inStream);
                        break;
                    case TokenTypes.DIV:
                        Token.Next(inStream);
                        left = left / Primary.Evaluate(inStream);
                        break;
                    default:
                        return left;
                }
            }
        }
    }
}