﻿using System.IO;

namespace CalculatorV2
{
    public class Expression
    {
        public static double Evaluate(TextReader inStream)
        {
            var left = Term.Evaluate(inStream);

            while (true)
            {
                switch (Token.Current)
                {
                    case TokenTypes.PLUS:
                        Token.Next(inStream);
                        left = left + Term.Evaluate(inStream);
                        break;
                    case TokenTypes.MINUS:
                        Token.Next(inStream);
                        left = left - Term.Evaluate(inStream);
                        break;
                    default:
                        return left;
                }
            }
        }
    }
}