﻿using System.IO;

namespace CalculatorV2
{
    public class Calculator
    {
        public Calculator()
        {
        }

        public void Evaluate(TextReader inStream, TextWriter outStream)
        {
            ExpressionList.Evaluate(inStream, outStream);
        }
    }
}