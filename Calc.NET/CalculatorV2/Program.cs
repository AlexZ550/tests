﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorV2
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello! Please, input math experession and press Enter 2 times to calculate.\n" +
                              "e.g.\n>x = 2 + 3<Enter>\n>y = 2 * (x + 5)<Enter>\n><Enter>\n5\n20\n" +
                              "To exit, please, press Ctrl+C");
            while(true)
            {
                var input_line = string.Empty;
                var user_input = string.Empty;

                do
                {
                    Console.Write(">");
                    input_line = Console.ReadLine();
                    user_input += input_line + Environment.NewLine;
                } while (!string.IsNullOrEmpty(input_line));

                var inStream = new StringReader(user_input);
                var outStream = Console.Out;
                var calc = new Calculator();
                calc.Evaluate(inStream, outStream);
            }
        }
    }
}
