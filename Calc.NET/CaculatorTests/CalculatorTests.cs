using CalculatorV2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace CaculatorTests
{
    [TestClass]
    public class CaclulatorTests
    {
        Calculator calc;
        
        [TestInitialize]
        public void TestInit()
        {
              calc = new Calculator();
        }

        [TestMethod]
        public void SimpleExpression1()
        {
            var user_input = "  x= 2 +2";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual("4\r\n", outStream.ToString());
            
        }

        [TestMethod]
        public void SimpleExpression2()
        {
            var user_input = "x=1";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual("1\r\n", outStream.ToString());

        }

        [TestMethod]
        public void SimpleExpression3()
        {
            var user_input = "x=1\n\n";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual("1\r\n", outStream.ToString());

        }

        [TestMethod]
        public void ComplexExpression4()
        {
            var user_input = "  x= 2 +2 ; z = x * 5\nz   = z * 5";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual("4\r\n20\r\n100\r\n", outStream.ToString());
        }

        [TestMethod]
        public void ComplexExpression5()
        {
            var user_input = "  x= (2 +2) *(10*10 +2) ; z = x * 5\nz   = z * 5";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual("408\r\n2040\r\n10200\r\n", outStream.ToString());
        }

        [TestMethod]
        public void ComplexExpression6()
        {
            var user_input = "  x= (2 +2) *(10*10 +2.3);;;";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual("409.2\r\n", outStream.ToString());
        }

        [TestMethod]
        public void InvilidExpression1()
        {
            var user_input = "  x= (2 +2 ; z = x * 5\nz   = z * 5";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual(") is expected\r\n z = x * 5\nz   = z * 5\r\n", outStream.ToString());
        }

        [TestMethod]
        public void InvilidExpression2()
        {
            var user_input = "  x= (2 +2.2.4) *(10*10 +2.3);;;";
            var inStream = new StringReader(user_input);
            var outStream = new StringWriter();
            calc.Evaluate(inStream, outStream);
            Assert.AreEqual("Input string was not in a correct format.\r\n) *(10*10 +2.3);;;\r\n", outStream.ToString());
        }

    }
}
